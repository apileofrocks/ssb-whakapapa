const test = require('tape')
const Server = require('../../test-bot')

test('view/get', t => {
  const server = Server()

  // profiles
  const bob = '%father/ajjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const lawrence = '%grandfather/2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'

  // publish initial state
  const createDetails = {
    name: 'Irving Family',
    focus: bob,
    mode: 'descendants',
    permission: 'view',
    ignoredProfiles: {
      add: [
        '%estranged/relative/Suf93EMK7AvU00kQt/3qxehc=.sha256',
        '%dave/asdasdasdasdasdasd3EMK7AvU00kQt/3qxehc=.sha256'
      ]
    },
    authors: {
      add: [server.id]
    },
    recordCount: 100,
    recps: [server.id]
  }
  server.whakapapa.view.create(createDetails, (err, viewId) => {
    t.equal(err, null, 'creates root without error')

    server.whakapapa.view.get(viewId, (_, data) => {
      delete data.states
      delete data.originalAuthor

      const expected = state({
        key: viewId,
        name: 'Irving Family',
        focus: bob,
        mode: 'descendants',
        permission: 'view',
        ignoredProfiles: [
          '%estranged/relative/Suf93EMK7AvU00kQt/3qxehc=.sha256',
          '%dave/asdasdasdasdasdasd3EMK7AvU00kQt/3qxehc=.sha256'
        ].sort(),
        recordCount: 100,
        recps: createDetails.recps,
        authors: {
          [server.id]: [{ start: 0, end: null }]
        },
        conflictFields: [],

        /* legacy */
        viewId
      })
      t.deepEqual(data, expected, 'gets reduced state of view')

      // publish update to state
      const updateDetails = {
        focus: lawrence,
        description: 'this is all descendants of lawrence irving',
        ignoredProfiles: { remove: ['%dave/asdasdasdasdasdasd3EMK7AvU00kQt/3qxehc=.sha256'] },
        permission: 'edit',
        image: {
          blob: '&07xxrn0wWbtRx/gp4IF7THPeWZUT1LWt93IVHTbitFE=.sha256',
          mimeType: 'image/png'
        },
        tombstone: {
          date: Date.now(),
          reason: 'oops'
        },
        recordCount: 101
      }
      server.whakapapa.view.update(viewId, updateDetails, (err) => {
        t.equal(err, null, 'creates update without error')

        server.whakapapa.view.get(viewId, (_, data) => {
          delete data.states
          delete data.originalAuthor

          const expected = state({
            key: viewId,
            name: 'Irving Family',
            description: 'this is all descendants of lawrence irving',
            focus: lawrence,
            mode: 'descendants',
            permission: 'edit',
            image: {
              blob: '&07xxrn0wWbtRx/gp4IF7THPeWZUT1LWt93IVHTbitFE=.sha256',
              mimeType: 'image/png'
            },
            tombstone: {
              date: updateDetails.tombstone.date,
              reason: 'oops'
            },
            ignoredProfiles: ['%estranged/relative/Suf93EMK7AvU00kQt/3qxehc=.sha256'],
            recps: createDetails.recps,
            recordCount: 101,
            authors: {
              [server.id]: [{ start: 0, end: null }]
            },
            conflictFields: [],

            /* legacy */
            viewId
          })
          t.deepEqual(data, expected, 'gets updated reduced state of view')

          server.close()
          t.end()
        })
      })
    })
  })
})

test('view/get (empty ignoredProfiles)', t => {
  const server = Server()

  // profiles
  const bob = '%father/ajjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'

  // publish initial state
  const createDetails = {
    name: { set: 'Irving Family' },
    focus: { set: bob },
    mode: { set: 'descendants' },
    recps: [server.id],
    recordCount: { set: 1 },
    authors: {
      add: [server.id]
    },
    permission: { set: 'submit' }
  }

  server.whakapapa.view.create(createDetails, (err, viewId) => {
    t.equal(err, null, 'creates root without error')

    server.whakapapa.view.get(viewId, (_, data) => {
      delete data.states
      delete data.originalAuthor

      const expected = state({
        key: viewId,
        name: 'Irving Family',
        focus: bob,
        mode: 'descendants',
        recps: createDetails.recps,
        recordCount: 1,
        authors: {
          [server.id]: [{ start: 0, end: null }]
        },
        conflictFields: [],
        permission: 'submit',

        /* legacy */
        viewId
      })
      t.deepEqual(data, expected, 'gets reduced state of view')

      server.close()
      t.end()
    })
  })
})

function state (overrides = {}) {
  const base = {
    type: 'whakapapa/view',

    name: null,
    description: null,
    image: null,

    focus: null,
    mode: null,
    ignoredProfiles: [],
    recordCount: null,
    importantRelationships: {},
    permission: null,

    tombstone: null,

    recps: null
  }

  return { ...base, ...overrides }
}
