const test = require('tape')
const Server = require('../../test-bot')

const type = 'link/story-artefact'
const parent = '%story/lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256' // a story id
const child = '%artefact/Pu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256' // an artefact id

test('link/tombstone (success)', t => {
  const server = Server()

  const core = { type, parent, child }
  const details = { recps: [server.id] }

  server.whakapapa.link.create(core, details, (err, linkId) => {
    if (err) throw err

    server.whakapapa.link.tombstone(linkId, { reason: 'woops' }, (err, updateId) => {
      if (err) throw err

      server.whakapapa.link.get(linkId, (err, link) => {
        if (err) throw err

        const state = {
          tombstone: {
            reason: 'woops',
            date: link.states[0].tombstone.date // hack
          }
        }

        t.deepEqual(
          link,
          {
            key: linkId,
            ...core,
            originalAuthor: server.id,
            recps: details.recps,
            conflictFields: [],
            states: [{
              key: updateId,
              ...state
            }],
            ...state
          }
        )

        server.close()
        t.end()
      })
    })
  })
})
