const test = require('tape')
const pull = require('pull-stream')
const Server = require('../test-bot')

test('get', t => {
  const server = Server()

  // profiles
  const barbie = '%parent/ajjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const bob = '%parent/bwrwerwerwerSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const mix = '%me/4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const ziva = '%child/AeXPu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256'

  const relationships = [
    {
      parent: barbie,
      child: mix,
      opts: {
        relationshipType: 'whangai'
      }
    },
    {
      parent: bob,
      child: mix
    },
    {
      parent: mix,
      child: ziva,
      opts: {
        relationshipType: { set: 'birth' }
      }
    }
  ]

  pull(
    pull.values(relationships),
    pull.asyncMap(({ parent, child, opts }, cb) => {
      server.whakapapa.child.create({ parent, child }, opts, cb)
    }),
    pull.collect((err, ids) => {
      if (err) throw err

      const expected = {
        id: mix,
        parentLinks: [
          { id: barbie, linkId: ids[0], relationshipType: 'whangai', legallyAdopted: null, tombstone: null },
          { id: bob, linkId: ids[1], relationshipType: null, legallyAdopted: null, tombstone: null }
        ],
        childLinks: [
          { id: ziva, linkId: ids[2], relationshipType: 'birth', legallyAdopted: null, tombstone: null }
        ]
      }
      normalise(expected)

      server.whakapapa.get(mix, (err, data) => {
        if (err) throw err

        normalise(data)
        t.deepEquals(data, expected, 'gets reduced state of all relationships')

        // test and update!
        const update = {
          legallyAdopted: { set: true }
        }

        server.whakapapa.child.update(ids[0], update, (_) => {
          const expected = {
            id: mix,
            parentLinks: [
              { id: barbie, linkId: ids[0], relationshipType: 'whangai', legallyAdopted: true, tombstone: null },
              { id: bob, linkId: ids[1], relationshipType: null, legallyAdopted: null, tombstone: null }
            ],
            childLinks: [
              { id: ziva, linkId: ids[2], relationshipType: 'birth', legallyAdopted: null, tombstone: null }
            ]
          }
          normalise(expected)

          server.whakapapa.get(mix, (_, data) => {
            normalise(data)
            t.deepEqual(data, expected, 'gets (updated) reduced state of all relationships')
            server.close()
            t.end()
          })
        })
      })
    })
  )
})

function normalise (result) {
  result.parentLinks = result.parentLinks.sort((a, b) => a - b)

  result.parentLinks.forEach(link => {
    delete link.parent
    delete link.child
    if (link.recps === null) { delete link.recps }
  })
  result.childLinks.forEach(link => {
    delete link.parent
    delete link.child
    if (link.recps === null) { delete link.recps }
  })
}
